import 'tiles/quote_tile/quote_tile_controller.dart';
import 'package:torowebsocket/app/modules/quotes/quotes_module.dart';
import 'package:torowebsocket/app/modules/sign_in/sign_in_module.dart';
import 'package:torowebsocket/app/modules/sign_up/sign_up_module.dart';
import 'package:torowebsocket/app/modules/splash/splash_page.dart';
import 'modules/splash/splash_controller.dart';
import 'app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:torowebsocket/app/app_widget.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds =>
      [$QuoteTileController, $SplashController, $AppController];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, __) => SplashPage()),
        ModularRouter("/signIn", module: SignInModule()),
        ModularRouter("/signUp", module: SignUpModule()),
        ModularRouter("/quotes", module: QuotesModule())
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
