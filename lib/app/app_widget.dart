import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:torowebsocket/core/constants/app.dart';
import 'package:torowebsocket/core/theme/theme.dart';

import 'app_controller.dart';

class AppWidget extends StatelessWidget {
 
   AppController controller = Modular.get();

 
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: Modular.navigatorKey,
      title: AppConst.APP_NAME,
      theme: controller.isDarkTheme?AppTheme.darkTheme:AppTheme.lightTheme,
      debugShowCheckedModeBanner: false,
      
      initialRoute: '/',
      onGenerateRoute: Modular.generateRoute,
    );
  }
}
