import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:torowebsocket/core/constants/app.dart';
import 'package:torowebsocket/core/database/shared_preferences.dart';

part 'app_controller.g.dart';

@Injectable()
class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {
  
  @observable
  bool isDarkTheme = false;

  var database = SharedPreferencesDB();

  init()async{
   
    var themeDB = await database.getBool(AppConst.THEMEISDARK);
    if(themeDB != null){
      isDarkTheme = themeDB;
    }

  }

 @action
  changeTheme(bool value) {
    isDarkTheme = value;
    database.saveBool(AppConst.THEMEISDARK, isDarkTheme);
    print(isDarkTheme);
  }

  _AppControllerBase(){
    init();
  }

}
