// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'custom_input_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $CustomInputController = BindInject(
  (i) => CustomInputController(
      label: i<String>(),
      errorText: i<String>(),
      icon: i<IconData>(),
      callback: i<Function>(),
      isValid: i<bool>(),
      ctr: i<dynamic>(),
      obscure: i<bool>(),
      txtType: i<TextInputType>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CustomInputController on _CustomInputControllerBase, Store {
  final _$errorTextAtom = Atom(name: '_CustomInputControllerBase.errorText');

  @override
  String get errorText {
    _$errorTextAtom.reportRead();
    return super.errorText;
  }

  @override
  set errorText(String value) {
    _$errorTextAtom.reportWrite(value, super.errorText, () {
      super.errorText = value;
    });
  }

  final _$labelAtom = Atom(name: '_CustomInputControllerBase.label');

  @override
  String get label {
    _$labelAtom.reportRead();
    return super.label;
  }

  @override
  set label(String value) {
    _$labelAtom.reportWrite(value, super.label, () {
      super.label = value;
    });
  }

  final _$iconAtom = Atom(name: '_CustomInputControllerBase.icon');

  @override
  IconData get icon {
    _$iconAtom.reportRead();
    return super.icon;
  }

  @override
  set icon(IconData value) {
    _$iconAtom.reportWrite(value, super.icon, () {
      super.icon = value;
    });
  }

  final _$callbackAtom = Atom(name: '_CustomInputControllerBase.callback');

  @override
  Function get callback {
    _$callbackAtom.reportRead();
    return super.callback;
  }

  @override
  set callback(Function value) {
    _$callbackAtom.reportWrite(value, super.callback, () {
      super.callback = value;
    });
  }

  final _$isValidAtom = Atom(name: '_CustomInputControllerBase.isValid');

  @override
  bool get isValid {
    _$isValidAtom.reportRead();
    return super.isValid;
  }

  @override
  set isValid(bool value) {
    _$isValidAtom.reportWrite(value, super.isValid, () {
      super.isValid = value;
    });
  }

  final _$ctrAtom = Atom(name: '_CustomInputControllerBase.ctr');

  @override
  dynamic get ctr {
    _$ctrAtom.reportRead();
    return super.ctr;
  }

  @override
  set ctr(dynamic value) {
    _$ctrAtom.reportWrite(value, super.ctr, () {
      super.ctr = value;
    });
  }

  final _$txtTypeAtom = Atom(name: '_CustomInputControllerBase.txtType');

  @override
  TextInputType get txtType {
    _$txtTypeAtom.reportRead();
    return super.txtType;
  }

  @override
  set txtType(TextInputType value) {
    _$txtTypeAtom.reportWrite(value, super.txtType, () {
      super.txtType = value;
    });
  }

  final _$obscureAtom = Atom(name: '_CustomInputControllerBase.obscure');

  @override
  bool get obscure {
    _$obscureAtom.reportRead();
    return super.obscure;
  }

  @override
  set obscure(bool value) {
    _$obscureAtom.reportWrite(value, super.obscure, () {
      super.obscure = value;
    });
  }

  final _$_CustomInputControllerBaseActionController =
      ActionController(name: '_CustomInputControllerBase');

  @override
  dynamic inputChange(dynamic value) {
    final _$actionInfo = _$_CustomInputControllerBaseActionController
        .startAction(name: '_CustomInputControllerBase.inputChange');
    try {
      return super.inputChange(value);
    } finally {
      _$_CustomInputControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
errorText: ${errorText},
label: ${label},
icon: ${icon},
callback: ${callback},
isValid: ${isValid},
ctr: ${ctr},
txtType: ${txtType},
obscure: ${obscure}
    ''';
  }
}
