import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:torowebsocket/core/theme/colors.dart';
import 'custom_input_controller.dart';

class CustomInputWidget extends StatelessWidget {
 
 CustomInputController controller;
 
 CustomInputWidget({@required this.controller});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16,right: 16,top: 10),
      child: Observer(
        builder: (_){
          return TextField(
            controller: controller.ctr,
              obscureText: controller.obscure,
              keyboardType: controller.txtType,
              onChanged: controller.inputChange,
              style: TextStyle(
                fontSize: 14.0,
                color: AppColors.primary,
              ),
              decoration: InputDecoration(
                  errorText: controller.isValid?null:controller.errorText,
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  prefixIcon: Icon(controller.icon,color: AppColors.primary,),
  
                  hintText: controller.label,
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blueAccent, width: 32.0),
                      borderRadius: BorderRadius.circular(25.0)),
                  focusedBorder: OutlineInputBorder(

                      borderSide: BorderSide(color: AppColors.primary, width: 1.0),
                      borderRadius: BorderRadius.circular(25.0)
                      
                      )
                      
                      )
                      
                      );
        },
      ),
    );
  }
}
