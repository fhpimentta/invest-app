import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'custom_input_controller.g.dart';

@Injectable()
class CustomInputController = _CustomInputControllerBase
    with _$CustomInputController;

abstract class _CustomInputControllerBase with Store {
  
  @observable
   String errorText;

  @observable
   String label;

  @observable  
   IconData icon;

  @observable 
  Function callback;

  @observable
  bool  isValid;

 
  @observable
  var ctr; 

  @observable 
  TextInputType txtType;
 
  @observable 
  bool obscure;

  @action 
  inputChange(value)=>callback(value);
  

_CustomInputControllerBase({
 @required this.label,
 @required this.errorText,
 @required this.icon,
 @required this.callback,
 @required this.isValid,
 @required this.ctr,
 @required this.obscure,
 @required this.txtType});
    

}
