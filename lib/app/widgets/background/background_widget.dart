import 'package:flutter/material.dart';
import 'package:torowebsocket/core/theme/colors.dart';

class BackgroundWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Column(children: [
            Expanded(flex: 1,child: Container(color: AppColors.background,),),
            Expanded(flex: 1,child: Container(color: Colors.white,),)],);

  }
}
