import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'background_controller.g.dart';

@Injectable()
class BackgroundController = _BackgroundControllerBase
    with _$BackgroundController;

abstract class _BackgroundControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
