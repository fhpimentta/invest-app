import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'custom_button_controller.g.dart';

@Injectable()
class CustomButtonController = _CustomButtonControllerBase
    with _$CustomButtonController;

abstract class _CustomButtonControllerBase with Store {
  @observable
  Function callback;

  @observable
  String label;


  _CustomButtonControllerBase({
    @required this.callback,
    @required this.label
  });
}
