import 'package:flutter/material.dart';
import 'package:torowebsocket/core/theme/colors.dart';

import 'custom_button_controller.dart';

class CustomButtonWidget extends StatelessWidget {
 
 CustomButtonController controller;
 
 CustomButtonWidget({@required this.controller});
 
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
        side: BorderSide(color: AppColors.primary)),
      color: AppColors.primary,
      textColor: Colors.white,
      padding: EdgeInsets.only(left: 50,right: 50),
      onPressed: controller.callback,
      child: Text(
        controller.label,
        style: TextStyle(
          fontSize: 14.0,
        ),
      ),
    );
  }
}
