// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'custom_button_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $CustomButtonController = BindInject(
  (i) => CustomButtonController(callback: i<Function>(), label: i<String>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CustomButtonController on _CustomButtonControllerBase, Store {
  final _$callbackAtom = Atom(name: '_CustomButtonControllerBase.callback');

  @override
  Function get callback {
    _$callbackAtom.reportRead();
    return super.callback;
  }

  @override
  set callback(Function value) {
    _$callbackAtom.reportWrite(value, super.callback, () {
      super.callback = value;
    });
  }

  final _$labelAtom = Atom(name: '_CustomButtonControllerBase.label');

  @override
  String get label {
    _$labelAtom.reportRead();
    return super.label;
  }

  @override
  set label(String value) {
    _$labelAtom.reportWrite(value, super.label, () {
      super.label = value;
    });
  }

  @override
  String toString() {
    return '''
callback: ${callback},
label: ${label}
    ''';
  }
}
