import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:torowebsocket/core/models/quote.dart';

part 'quote_tile_controller.g.dart';

@Injectable()
class QuoteTileController = _QuoteTileControllerBase with _$QuoteTileController;

abstract class _QuoteTileControllerBase with Store {
 
  @observable
  Quote quote;

  @observable
  Map<DateTime, double> chart = Map<DateTime, double> ();

  @action 
  addChart({dateTime:DateTime,val:double})async{
    chart.putIfAbsent(dateTime, () => val);
  }

  _QuoteTileControllerBase({@required this.quote});
 
}
