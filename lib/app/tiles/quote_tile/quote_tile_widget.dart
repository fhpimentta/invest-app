import 'package:fl_animated_linechart/fl_animated_linechart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:slimy_card/slimy_card.dart';
import 'package:torowebsocket/app/tiles/quote_tile/quote_tile_controller.dart';
import 'package:torowebsocket/core/theme/colors.dart';

class QuoteTileWidget extends StatelessWidget {
 
  QuoteTileController controller;

  QuoteTileWidget({@required this.controller});
 
  @override
  Widget build(BuildContext context) {
      return Padding(
          padding: const EdgeInsets.only(top:8.0,left: 20,right: 20),
          child:SlimyCard(
                color: AppColors.background,
                topCardHeight: 150,
                bottomCardHeight: 150,
                borderRadius: 15,
                topCardWidget: cardInfo(),
                bottomCardWidget: cardCarts(),
                slimeEnabled: true ,
              ),
        );
  }

  Widget cardCarts(){

          return  Observer(
            builder: (_){
              return controller.chart.length > 7
              ?Container(
                width: 300,
                height: 200,
                child: AnimatedLineChart(
                      LineChart.fromDateTimeMaps(
                        [controller.chart], [AppColors.primary], ['C'],
                        tapTextFontWeight: FontWeight.w400),
                      key: UniqueKey(),
                    ),
              )
              :Center(child: CircularProgressIndicator(),);
            },
          );
  }



  Widget cardInfo(){
    return  Card(
                color: AppColors.background,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(children: [
                    Expanded(child: Text(controller.quote.name,style: TextStyle(color: Colors.white,fontSize: 18),),flex: 3,),
                    SizedBox(width: 1,height: 80,child: Container(color: Colors.white,),),
                    
                    Expanded(child:controller.quote.value>controller.quote.oldValue
                    ?Icon(Icons.arrow_drop_up, color: Colors.green,size: 35,  )
                    :Icon(Icons.arrow_drop_down, color: Colors.red,size: 35,  ),flex: 1,),
                    Expanded(child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("R\$ ${controller.quote.value}",style: TextStyle(color: Colors.white,fontSize: 18),),
                    ),flex:4,),
                    Expanded(child: Text("${controller.quote.getDate().hour}:${controller.quote.getDate().minute}",style: TextStyle(color: Colors.white,fontSize: 16),),flex: 2,),
                  ],),
                ),
            );
  }

}
