// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quote_tile_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $QuoteTileController = BindInject(
  (i) => QuoteTileController(quote: i<Quote>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$QuoteTileController on _QuoteTileControllerBase, Store {
  final _$quoteAtom = Atom(name: '_QuoteTileControllerBase.quote');

  @override
  Quote get quote {
    _$quoteAtom.reportRead();
    return super.quote;
  }

  @override
  set quote(Quote value) {
    _$quoteAtom.reportWrite(value, super.quote, () {
      super.quote = value;
    });
  }

  final _$chartAtom = Atom(name: '_QuoteTileControllerBase.chart');

  @override
  Map<DateTime, double> get chart {
    _$chartAtom.reportRead();
    return super.chart;
  }

  @override
  set chart(Map<DateTime, double> value) {
    _$chartAtom.reportWrite(value, super.chart, () {
      super.chart = value;
    });
  }

  final _$addChartAsyncAction =
      AsyncAction('_QuoteTileControllerBase.addChart');

  @override
  Future addChart({dynamic dateTime = DateTime, dynamic val = double}) {
    return _$addChartAsyncAction
        .run(() => super.addChart(dateTime: dateTime, val: val));
  }

  @override
  String toString() {
    return '''
quote: ${quote},
chart: ${chart}
    ''';
  }
}
