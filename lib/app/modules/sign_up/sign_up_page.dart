import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:torowebsocket/app/widgets/background/background_widget.dart';
import 'package:torowebsocket/core/theme/colors.dart';
import 'sign_up_controller.dart';
//TODO to implement
class SignUpPage extends StatefulWidget {
  final String title;
  const SignUpPage({Key key, this.title = "SignUp"}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends ModularState<SignUpPage, SignUpController> {
  //use 'controller' variable to access controller

 @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
      child: body(),
    ),);
  }


  Widget body(){
    return Stack(
      children: [
               BackgroundWidget(),

        ListView(children: [
          SizedBox(height: 40,),
          SvgPicture.asset(
            'assets/logos/logo-light.svg',
            semanticsLabel: 'Toro Logo',
            width: 130,
          ),
          Padding(
            padding: const EdgeInsets.only(left:45.0,right: 45,top: 25),
            child: Container(
              height: 800,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
              ),
              child: Column(
                children: [
                    
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Center(child: Text('Cadastre-se',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(child: Text('Preencha os campos abaixo e cadastre-se',style: TextStyle(fontSize: 11),),),
                    ),

                    input(label: "Nome",icon: Icons.mail_outline),
                    input(label: "E-mail",icon: Icons.mail_outline),
                    input(label: "Telefone",icon: Icons.mail_outline),
                    input(label: "Senha",icon: Icons.lock_outline),

                   
                  SizedBox(height: 30,),
                   
                   
                    button(),
                  
                  Padding(
                      padding: const EdgeInsets.only(top: 20,bottom: 5),
                      child:  Center(child: 
                      
                      RichText(
                        text: TextSpan(
                          text: 'Já é cadastrado? ',
                          style: TextStyle(fontSize: 11,color: AppColors.background),
                          children: <TextSpan>[
                            TextSpan(
                              text: 'Faça o login', style: TextStyle(color: AppColors.primary,),
                              recognizer:  TapGestureRecognizer()..onTap = () => Modular.to.pop(),
                              ),
                          
                          ],
                        ),
                      )
                      
                      ),
                      ),
                  ]
              ,),
              
              
              ),
          ),


        ],)
        


      ],
    );
  }

  Widget button(){
    return FlatButton(
      
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
        side: BorderSide(color: AppColors.primary)),
      color: AppColors.primary,
      textColor: Colors.white,
      padding: EdgeInsets.only(left: 50,right: 50),
      onPressed: () {},
      child: Text(
        "Acessar",
        style: TextStyle(
          fontSize: 14.0,
        ),
      ),
    );
  }

  Widget input({label:String,icon:Icons}){
    return Padding(
      padding: const EdgeInsets.only(left: 16,right: 16,top: 10),
      child: TextField(
              style: TextStyle(
                fontSize: 14.0,
                color: AppColors.primary,
              ),
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
  
                  hintText: label,
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blueAccent, width: 32.0),
                      borderRadius: BorderRadius.circular(25.0)),
                  focusedBorder: OutlineInputBorder(

                      borderSide: BorderSide(color: AppColors.primary, width: 1.0),
                      borderRadius: BorderRadius.circular(25.0)
                      
                      )
                      
                      )
                      
                      ),
    );
  }

  




}
