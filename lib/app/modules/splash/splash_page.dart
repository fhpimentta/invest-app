import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:torowebsocket/core/theme/colors.dart';
import 'splash_controller.dart';

class SplashPage extends StatefulWidget {
  final String title;
  const SplashPage({Key key, this.title = "Splash"}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends ModularState<SplashPage, SplashController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
 return Scaffold(
      body: Container(
        color: AppColors.background,
        child: Center(child:
      SvgPicture.asset(
          'assets/logos/logo-light.svg',
          semanticsLabel: 'Toro Logo',
          width: 300,
        )
      ),
      )
    );
  }
}
