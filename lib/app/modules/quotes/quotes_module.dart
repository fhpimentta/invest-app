import 'package:torowebsocket/app/modules/quotes/quotes_page.dart';

import 'quotes_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

class QuotesModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $QuotesController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, __) => QuotesPage()),
      ];

  static Inject get to => Inject<QuotesModule>.of();
}
