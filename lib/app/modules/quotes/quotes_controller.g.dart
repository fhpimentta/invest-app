// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quotes_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $QuotesController = BindInject(
  (i) => QuotesController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$QuotesController on _QuotesControllerBase, Store {
  final _$listQuotesWidgetAtom =
      Atom(name: '_QuotesControllerBase.listQuotesWidget');

  @override
  ObservableList<QuoteTileController> get listQuotesWidget {
    _$listQuotesWidgetAtom.reportRead();
    return super.listQuotesWidget;
  }

  @override
  set listQuotesWidget(ObservableList<QuoteTileController> value) {
    _$listQuotesWidgetAtom.reportWrite(value, super.listQuotesWidget, () {
      super.listQuotesWidget = value;
    });
  }

  final _$quotesAtom = Atom(name: '_QuotesControllerBase.quotes');

  @override
  Map<String, Quote> get quotes {
    _$quotesAtom.reportRead();
    return super.quotes;
  }

  @override
  set quotes(Map<String, Quote> value) {
    _$quotesAtom.reportWrite(value, super.quotes, () {
      super.quotes = value;
    });
  }

  final _$channelAtom = Atom(name: '_QuotesControllerBase.channel');

  @override
  IOWebSocketChannel get channel {
    _$channelAtom.reportRead();
    return super.channel;
  }

  @override
  set channel(IOWebSocketChannel value) {
    _$channelAtom.reportWrite(value, super.channel, () {
      super.channel = value;
    });
  }

  final _$_QuotesControllerBaseActionController =
      ActionController(name: '_QuotesControllerBase');

  @override
  dynamic updateList({dynamic key = String, dynamic quote = Quote}) {
    final _$actionInfo = _$_QuotesControllerBaseActionController.startAction(
        name: '_QuotesControllerBase.updateList');
    try {
      return super.updateList(key: key, quote: quote);
    } finally {
      _$_QuotesControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addList({dynamic key = String, dynamic quote = Quote}) {
    final _$actionInfo = _$_QuotesControllerBaseActionController.startAction(
        name: '_QuotesControllerBase.addList');
    try {
      return super.addList(key: key, quote: quote);
    } finally {
      _$_QuotesControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic close() {
    final _$actionInfo = _$_QuotesControllerBaseActionController.startAction(
        name: '_QuotesControllerBase.close');
    try {
      return super.close();
    } finally {
      _$_QuotesControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listQuotesWidget: ${listQuotesWidget},
quotes: ${quotes},
channel: ${channel}
    ''';
  }
}
