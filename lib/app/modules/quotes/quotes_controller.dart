import 'dart:convert';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:torowebsocket/app/tiles/quote_tile/quote_tile_controller.dart';
import 'package:torowebsocket/core/models/quote.dart';
import 'package:web_socket_channel/io.dart';

part 'quotes_controller.g.dart';

@Injectable()
class QuotesController = _QuotesControllerBase with _$QuotesController;

abstract class _QuotesControllerBase with Store {
  

  @observable
  ObservableList<QuoteTileController> listQuotesWidget = [QuoteTileController(quote:Quote(timestamp: 0.0,name: "",value: 0.0) )].asObservable();
  
  @observable
  Map<String,Quote> quotes = Map();

  @observable
  IOWebSocketChannel channel;

  @observable
  var filter = "";

  @computed
  List<QuoteTileController> get listFilter {
    if(filter.isEmpty){
      return listQuotesWidget;
    }else{
      return listQuotesWidget.where((i)=> i.quote.name.toLowerCase().contains(filter.toLowerCase())).toList();
    }
  }


  @action
  setFileter(String value) => filter = value;


//init websocket
   init()async{
    listQuotesWidget.clear();
    channel = IOWebSocketChannel.connect('ws://localhost:8080/quotes');
    channel.stream.listen((message) {
        channel.sink.add("received!");
        getData(message);
    }); 

  }

_QuotesControllerBase(){init();}

//convert data to Quote
  getData(val){
      Map m = (json.decode(val));
      List values = m.values.toList();
      List keys = m.keys.toList(); 
      Quote quote = Quote(name: keys[0],value: values[0],timestamp: values[1]);
      quotes.containsKey(keys[0])?updateList(key: keys[0],quote: quote):addList(key: keys[0],quote: quote);
    print("Chave:${quote.name}:${quote.value} at time: ${quote.getDate().toIso8601String()}");
  }

//update quote value and time
  @action
  updateList({key:String,quote:Quote}){
      quote.oldValue = quotes[key].value;
      quotes.update(key, (value) => quote);
      ObservableList<QuoteTileController> auxList =  listQuotesWidget;
      auxList.firstWhere((q) { 
        if(q.quote.name ==key){
            q.quote = quote;
            q.addChart(
              dateTime:q.quote.getDate(), val: q.quote.value
            );
            return true;
          } 
          return false;
          });
          auxList.sort((a,b)=>b.quote.value.compareTo(a.quote.value));
          listQuotesWidget=auxList;

  }
//add quote value and time
  @action
  addList({key:String,quote:Quote}){
    quote.oldValue = 0.0;
    quotes.putIfAbsent(key, () => quote);
    var ctr = QuoteTileController(quote: quote);
    ctr.addChart(
      dateTime:quote.getDate(), val: quote.value
    );
    listQuotesWidget.add(ctr);
    
  }

// close screen
  @action
  close(){
    channel.sink.close();
    Modular.to.pushReplacementNamed('/');
  }

  
}
