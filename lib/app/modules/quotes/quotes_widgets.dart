import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:torowebsocket/app/modules/quotes/quotes_controller.dart';
import 'package:torowebsocket/app/widgets/custom_input/custom_input_controller.dart';
import 'package:torowebsocket/app/widgets/custom_input/custom_input_widget.dart';

class QuotesWidgets{

QuotesController controller = Modular.get();

Widget topBar(){
  return  Align(
            alignment: Alignment.topCenter,
                      child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SvgPicture.asset(
                'assets/logos/logo-light.svg',
                semanticsLabel: 'Toro Logo',
                width: 200,
                alignment: Alignment.center,
              ),
            ),
          );
}

Widget closeButton(){
  return Align(
            alignment: Alignment.topLeft,
            child: GestureDetector(
              onTap: (){
                controller.close();
              },
              child:Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(Icons.close,color: Colors.white,size: 35,),
              ) ,
              
              ),
            
            );
}



Widget searchInput(){
  return  Padding(
          padding: const EdgeInsets.only(top:70),
          child: Observer(
            builder: (_){
                return CustomInputWidget(
                  controller: CustomInputController(
                    txtType: TextInputType.name,
                    label: "Procurar",
                    icon:Icons.search,
                    errorText: "",
                    isValid: true,
                    callback: controller.setFileter,
                    obscure: false,
                    
                  ));
                
                
          },),
        );
}
  
}