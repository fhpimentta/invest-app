
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:torowebsocket/app/modules/quotes/quotes_widgets.dart';
import 'package:torowebsocket/app/tiles/quote_tile/quote_tile_widget.dart';
import 'package:torowebsocket/app/widgets/background/background_widget.dart';
import 'package:torowebsocket/core/theme/colors.dart';
import 'quotes_controller.dart';


class QuotesPage extends StatefulWidget {
  const QuotesPage({Key key}) : super(key: key);

  @override
  _QuotesPageState createState() => _QuotesPageState();
}


class _QuotesPageState extends ModularState<QuotesPage, QuotesController>  with QuotesWidgets{
  //use 'controller' variable to access controller

 
 @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
      child: body(),
    ),);
  }

 Widget body(){
    return Stack(
      children: [
          Column(children: [
            Expanded(flex: 1,child: Container(color: AppColors.background,),),
            Expanded(flex: 8,child: Container(color: Colors.white,),)],),
        
        searchInput(),
        listQuotes(),
        topBar(),
        closeButton(), 
      ],
    );
 }



  Widget listQuotes(){
    return    
        Padding(
          padding: const EdgeInsets.only(top:140),
          child: Observer(
            builder: (_){
                  return ListView.builder(
                    itemCount: controller.listFilter.length,
                    itemBuilder: (_,index){
                      return QuoteTileWidget(controller: controller.listFilter[index],);
                    },
            );
            },
          )
        );

  }

}
