import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:torowebsocket/app/widgets/background/background_widget.dart';
import 'package:torowebsocket/app/widgets/custom_button/custom_button_controller.dart';
import 'package:torowebsocket/app/widgets/custom_button/custom_button_widget.dart';
import 'package:torowebsocket/app/widgets/custom_input/custom_input_controller.dart';
import 'package:torowebsocket/app/widgets/custom_input/custom_input_widget.dart';
import 'package:torowebsocket/core/theme/colors.dart';
import 'sign_in_controller.dart';

class SignInPage extends StatefulWidget {
  final String title;
  const SignInPage({Key key, this.title = "SignIn"}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends ModularState<SignInPage, SignInController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      resizeToAvoidBottomPadding: false,
      body: SafeArea(
      child: body(),
    ),);
  }


  Widget body(){
    return Stack(
      children: [
       BackgroundWidget(),
        ListView(children: [
          SizedBox(height: 100,),
          SvgPicture.asset(
            'assets/logos/logo-light.svg',
            semanticsLabel: 'Toro Logo',
            width: 200,
          ),
          Padding(
            padding: const EdgeInsets.all(45.0),
            child: Container(
              height: 400,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
              ),
              child: Column(
                children: [
                    
                    Padding(
                      padding: const EdgeInsets.only(top: 20,bottom: 5),
                      child: Center(child: Text('Bem Vindo!',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),),
                    ),
                    Center(child: Text('Insira abaixo seus dados de login',style: TextStyle(fontSize: 11),),),

                    
                   Observer(
                     builder: (_){
                       return  CustomInputWidget(controller: CustomInputController(
                      label: "E-mail",
                      errorText: "E-mail invalido",
                      icon: Icons.mail_outline,
                      callback: controller.emailChange,
                      obscure: false,
                      txtType: TextInputType.emailAddress,
                      isValid:controller.emailValidate
                      ),);
                     },
                   ),
                    
                   Observer(
                     builder: (_){
                       return   CustomInputWidget(controller: CustomInputController(
                      label: "Senha",
                      icon: Icons.lock_outline,
                      callback: controller.passwordChange,
                      obscure: true,
                      txtType: TextInputType.text,
                      errorText: "Senha deve ter 6 digitos no minimo",
                      isValid:controller.passwordValidate
                    

                      ),);
                     },
                   ),
                    
                    
                  

                   
                 Padding(
                      padding: const EdgeInsets.only(top: 20,bottom: 5),
                      child:  Center(child: 
                      
                      RichText(
                        text: TextSpan(
                          text: 'Esqueceu senha? ',
                          style: TextStyle(fontSize: 11,color: AppColors.background),
                          children: <TextSpan>[
                            TextSpan(
                              text: 'Recuperar aqui', style: TextStyle(color: AppColors.primary,),
                              recognizer:  TapGestureRecognizer()..onTap = () => print('not implemented'),
                              ),
                          
                          ],
                        ),
                      )
                      
                      ),
                      ),
                   
                   
                    CustomButtonWidget(controller: CustomButtonController(label: "Acessar",callback: (){controller.submit(context);}),),

                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(child: Text('Não tem Cadastro?',style: TextStyle(fontSize: 11),),),
                    ),
                     Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(child: GestureDetector(
                        onTap: (){
                          Modular.to.pushNamed('/signUp');
                        },
                        child: Text('Cadastre-se gratuitamente',style: TextStyle(fontSize: 11,color: AppColors.primary),)),),
                    ),

                  ]
              ,),
              
              
              ),
          ),


        ],),
        

        Align(
          alignment: Alignment.bottomCenter,
          child:Image.asset('assets/images/toro.png',height: MediaQuery.of(context).size.height/5,)
          
          )
      ],
    );
  }

 

  

}
