import 'package:torowebsocket/app/modules/sign_in/sign_in_page.dart';

import 'sign_in_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SignInModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $SignInController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,child:(_,__)=> SignInPage()),

  ];

  static Inject get to => Inject<SignInModule>.of();
}
