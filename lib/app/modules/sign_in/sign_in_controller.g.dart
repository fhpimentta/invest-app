// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sign_in_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $SignInController = BindInject(
  (i) => SignInController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SignInController on _SignInControllerBase, Store {
  Computed<bool> _$emailValidateComputed;

  @override
  bool get emailValidate =>
      (_$emailValidateComputed ??= Computed<bool>(() => super.emailValidate,
              name: '_SignInControllerBase.emailValidate'))
          .value;
  Computed<bool> _$passwordValidateComputed;

  @override
  bool get passwordValidate => (_$passwordValidateComputed ??= Computed<bool>(
          () => super.passwordValidate,
          name: '_SignInControllerBase.passwordValidate'))
      .value;
  Computed<bool> _$canSubmitComputed;

  @override
  bool get canSubmit =>
      (_$canSubmitComputed ??= Computed<bool>(() => super.canSubmit,
              name: '_SignInControllerBase.canSubmit'))
          .value;

  final _$emailAtom = Atom(name: '_SignInControllerBase.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$passwordAtom = Atom(name: '_SignInControllerBase.password');

  @override
  String get password {
    _$passwordAtom.reportRead();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.reportWrite(value, super.password, () {
      super.password = value;
    });
  }

  final _$_SignInControllerBaseActionController =
      ActionController(name: '_SignInControllerBase');

  @override
  dynamic emailChange(String value) {
    final _$actionInfo = _$_SignInControllerBaseActionController.startAction(
        name: '_SignInControllerBase.emailChange');
    try {
      return super.emailChange(value);
    } finally {
      _$_SignInControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic passwordChange(String value) {
    final _$actionInfo = _$_SignInControllerBaseActionController.startAction(
        name: '_SignInControllerBase.passwordChange');
    try {
      return super.passwordChange(value);
    } finally {
      _$_SignInControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic submit(dynamic context) {
    final _$actionInfo = _$_SignInControllerBaseActionController.startAction(
        name: '_SignInControllerBase.submit');
    try {
      return super.submit(context);
    } finally {
      _$_SignInControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
email: ${email},
password: ${password},
emailValidate: ${emailValidate},
passwordValidate: ${passwordValidate},
canSubmit: ${canSubmit}
    ''';
  }
}
