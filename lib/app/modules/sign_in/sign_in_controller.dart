import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:torowebsocket/core/constants/app.dart';
import 'package:torowebsocket/core/theme/colors.dart';

part 'sign_in_controller.g.dart';

@Injectable()
class SignInController = _SignInControllerBase with _$SignInController;


abstract class _SignInControllerBase with Store {
  
  @observable
  String email;

  @observable 
  String password;


  @action
  emailChange(String value) => email = value;
 
  @action 
  passwordChange(String value) => password = value;
  
  @computed 
  bool get emailValidate => email!=null && email!= "" ?EmailValidator.validate(email):true;
  @computed 
  bool get passwordValidate => password!=null && password!= "" ?password.length>5:true;

  @computed 
  bool get canSubmit => emailValidate && passwordValidate;

  @action
  submit(context){

    if(email== null || password == null ){
      AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Erro!',
            desc: 'campos não preenchidos',
            btnOkColor: Colors.red,
            btnOkOnPress: () {},
            )..show();
    }else if(canSubmit){

      if(User.email == email && User.password == password){
        AwesomeDialog(
                    context: context,
                    dialogType: DialogType.SUCCES,
                    animType: AnimType.BOTTOMSLIDE,
                    title: 'Autenticado com sucesso!',
                    desc: '',
                    btnOkColor: AppColors.primary,
                    btnOkOnPress: () {
                          Modular.to.pushReplacementNamed('/quotes');
                    },
                    )..show();
      }else{
        AwesomeDialog(
            context: context,
            dialogType: DialogType.WARNING,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Acesso negado!',
            desc: 'login e senha inválidos',
            btnOkColor: Colors.red,
            btnOkOnPress: () {},
            )..show();
      }

    }


  }

}
