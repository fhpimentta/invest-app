import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:torowebsocket/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

void main() {

SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
   statusBarColor: Colors.white, // Color for Android
   statusBarBrightness: Brightness.dark // Dark == white status bar -- for IOS.
));
runApp(ModularApp(module: AppModule()));
}