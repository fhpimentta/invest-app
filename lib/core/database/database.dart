


abstract class Database {

  Future  get(table);

  Future save(table,values);

  Future clean();


  Future saveRow(table,value);

  Future getRow(table,query);

}