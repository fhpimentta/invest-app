import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'database.dart';

class SharedPreferencesDB implements Database{

  SharedPreferences pref;



  setPref()async{
    pref =  await SharedPreferences.getInstance();
  }

  @override
  Future get(table)async{
    await setPref();

    var data = await pref.get(table);
    if(data !=null) {
      return json.decode(data);
    }else{
      return null;
    }

  }
  Future saveBool(table, values)async{
    await setPref();
    pref.setBool(table, (values));
  }
  Future getBool(table)async{
    await setPref();

    var data =  pref.getBool(table);
    if(data !=null) {
      return (data);
    }else{
      return null;
    }

  }

  @override
  Future save(table, values)async{
    await setPref();
    pref.setString(table, json.encode(values));
  }

  @override
  Future saveString(table, values)async{
    await setPref();
    pref.setString(table,(values));
  }

  @override
  Future clean()async{
    await setPref();
    pref.clear();
  }




  @override
  Future getRow(table, query) async{
    await setPref();
    return pref.get(table);
  }

  @override
  Future saveRow(table, value) async{
    await setPref();
    pref.setString(table, value);
  }







}
