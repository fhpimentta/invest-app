import 'package:flutter/material.dart';

class AppColors{

  static Color background = Color.fromRGBO(13, 23, 34, 1);
  static Color primary = Color.fromRGBO(16, 163, 201, 1);

}