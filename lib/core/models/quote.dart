class Quote{
  String name;
  double oldValue;
  double value;
  double timestamp;

  Quote({this.name,this.value,this.timestamp,this.oldValue});

  getDate() => DateTime.fromMillisecondsSinceEpoch(this.timestamp.round() * 1000).toLocal();
  
}