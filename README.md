<h1 align="center">
	<img alt="InvesApp" src="/assets/logos/icon.png" width="200px" />
</h1>

<h3 align="center">
  Invest App
</h3>



## 📥 Instalação e execução

1. Faça um clone desse repositório.
2. Instalar <a href='https://flutter.dev/docs/get-started/install'>Flutter</a>
3. Instalar <a href='https://www.notion.so/Instalando-Docker-6290d9994b0b4555a153576a1d97bee2'>DOCKER</a>
4. Instalar (opcional) <a href='https://github.com/Flutterando/slidy'>Slidy</a>


### Banco de Dados
1. A partir da raiz executar `docker run -p 8080:8080 toroinvest/quotesmock`

### flutter

1. Rode `flutter pub get` para instalar as dependências;
2. Rode `flutter run` para iniciar o app.

### acesso
email:user@gmail.com
senha:123456

Feito com ♥ by [FernandoPimenta](https://www.linkedin.com/in/fepimenta/)
